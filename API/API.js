import axios from "axios"

export function getAllDonnee() {

    const url = 'http://52.143.158.205/api/gps'
    return new Promise((resolve, reject) => {
        return axios.get(url).then(response => {
            resolve(response)
            }).catch(error => {
            reject(error)
        });
    });
}


export function getOneDonnee() {

    const url = 'http://52.143.158.205/api/gpsOne'
    return new Promise((resolve, reject) => {
        return axios.get(url).then(response => {
            resolve(response)
            }).catch(error => {
            reject(error)
        });
    });
}

export function getAllUser() {

    const url = 'http://52.143.158.205/api/users'
    return new Promise((resolve, reject) => {
        return axios.get(url).then(response => {
            resolve(response)
            }).catch(error => {
            reject(error)
        });
    });
}

export function getCars(entreprise) {

    const url = 'http://52.143.158.205/api/cars/' + entreprise
    return new Promise((resolve, reject) => {
        return axios.get(url).then(response => {
            resolve(response)
            }).catch(error => {
            reject(error)
        });
    });
}