import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Home from '../Components/Home'
import Vehicles from '../Components/Vehicles'
import Vehicle from '../Components/Vehicle'


const LocalisNavigator = createStackNavigator({
    // Page d'accueil permettant d'afficher les résultats météo pour une ville donnée.
    Home: {
        screen: Home,
        navigationOptions: {
            title: 'Localis'
        }
    },
    Vehicles: {
        screen: Vehicles,
        navigationOptions: {
            title: 'Localis'
        }
    }
    ,
    Vehicle: {
        screen: Vehicle,
        navigationOptions: {
            title: 'Localis'
        }
    }
    
})

export default createAppContainer(LocalisNavigator)
