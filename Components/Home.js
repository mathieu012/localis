import React from 'react';
import { View, Text, StyleSheet, TextInput, Button, Keyboard} from 'react-native'
import { getAllUser } from '../API/API.js';
import UserBdd from '../Data/UsersBdd'


class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {userText: ''}
        this.state = {pwdText: ''}
        this.state = { donnee: [] }
        this.state = { user: [] }
        this.state = { pwd: [] }
        
    }
    componentDidMount(){
        
        //Retourne les utilsateurs de l'application par API
        getAllUser().then(res => {
            this.setState({ donnee: res.data })
            
        })
    }

    //Amene à la page liste de vehicules si les identifiants correspondent aux donnees de l'api
    goToVehicles = (user, pwd) => {
        
        console.log("1")
        for(const element of this.state.donnee) {
            if (user == element.user && element.password)
            this.props.navigation.navigate("Vehicles", {entreprise: element.user})
          }
        
    }
    render() {
        return (
            <View style={styles.content}>
                
                <View style={styles.connect}>
                    
                    <View style = {{ alignItems: 'center', flex: 1}}><Text style={styles.text}>Connexion</Text></View>
                    
                    <View style = {{  flex: 5 }}>

                        <Text style={styles.text}>User</Text>
                        <TextInput style={styles.input} onChangeText={(text) => this.setState({userText: text})}></TextInput>
                        
                        <Text style={styles.text}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true} onChangeText={(text) => this.setState({pwdText: text})}></TextInput>

                        <View style = {{ marginBottom: 20 }}></View>
                        <Button style={styles.button} title = 'validate' onPress={() => this.goToVehicles(this.state.userText, this.state.pwdText)}></Button>

                        <View style = {{ marginBottom: 20 }}></View>
                        <View style = {{ marginBottom: 10 }}><Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</Text></View>
                    
                    </View>
                    
                    <View style = {{ flex: 2 }}>
                        
                        
                    
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1
    },
    text: {
        fontSize: 20,
        
    },
    input: {
        width: 250,
        borderWidth: 1,
        borderRadius: 3,
        margin: 10
        
    },
    connect: {
        backgroundColor: '#F9F9F9',
        flex: 1,
        marginLeft: 50,
        marginRight: 50,
        marginBottom: 0,
        marginTop: 100,
        padding: 10,
        elevation: 10
        
    },
    button: {
        
    }
})
export default Home
