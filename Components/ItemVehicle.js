import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, Button } from 'react-native'
import VehicleBdd from '../Data/VehicleBdd.js'

class ItemVehicle extends React.Component {
    render() {
        const vehicle = this.props.vehicle
        return (
            <View style={styles.content}>
                
                    <View style={{ flex: 1}}><Image style={styles.img} source={{uri: vehicle.photo}}></Image></View>
                    <View style={{ flex: 2, alignItems: 'center'}}>
                        <Text>{vehicle.imatriculation}</Text>
                        <Button title = 'validate' onPress={() => this.props.goToVehicle(vehicle.id, vehicle.photo, vehicle.imatriculation)}></Button>
                    </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({

    content: {
        flex: 1,
        //alignItems: 'center',
        borderWidth: 2,
        borderRadius: 3,
        flexDirection: 'row',
        margin: 10,
        backgroundColor: 'white',
        elevation: 10
    },
    img: {
        width: 130,
        height: 100,
    },
    displayVehicle: {
       
        
    }

})

export default ItemVehicle
