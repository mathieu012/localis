import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native'
import VehicleBdd from '../Data/VehicleBdd.js'
import ItemVehicle from './ItemVehicle'
import { getCars } from '../API/API.js';

class Vehicles extends React.Component {

    constructor(props) {
        super(props)
        this.state = { donnee: [] }
           
    }

    componentDidMount(){

        //Retourne les voitures d'une entreprise par API
        getCars(this.props.navigation.state.params.entreprise).then(res => {
            this.setState({ donnee: res.data })

        })
    }

    //Amene à la page de geolocalisation du vehicule avec ces proprietees
    _goToVehicle = (idVoiture, photo, imatriculation) => {
        this.props.navigation.navigate("Vehicle", {voiture: [idVoiture, photo, imatriculation]})
    }

    render() {
        return (
            <View style={styles.content}>
            
                <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 10}}><Text style={{ fontSize: 18 }}>{this.props.navigation.state.params.entreprise}</Text><Text style={{ fontSize: 18 }}>Your Vehicles</Text></View>
                
                <View style={{ flex: 5}}>

                    <FlatList data={this.state.donnee}
                        keyExtractor={(item) => item._id}
                        renderItem={({item}) => <ItemVehicle vehicle={item} goToVehicle={this._goToVehicle}></ItemVehicle>}>
                    </FlatList>
                    
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({

    content: {
        flex: 1,
        //alignItems: 'center',
    }

})
export default Vehicles
