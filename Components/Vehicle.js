import React from 'react';
import { View, Text, StyleSheet, FlatList, Button, Image } from 'react-native'
import ItemVehicle from './ItemVehicle'
import { getOneDonnee } from '../API/API.js';
import openMap from 'react-native-open-maps';
import VehicleBdd from '../Data/VehicleBdd.js'

class Vehicle extends React.Component {

    constructor(
        props) {
        super(props)
        this.state = { donnee: [] }
        this.state = { long: [] }
        this.state = { lag: [] }
        this.state = { time: [] }
        
    }

    componentDidMount(){

        //Retourne une donnee GPS la plus recente par API
        getOneDonnee().then(res => {
            
            this.setState({ donnee: res.data })
            this.setState({ long: this.state.donnee[0].coordinates[0] })
            this.setState({ lag: this.state.donnee[0].coordinates[1] })
            
            //Convertion heure au format timetamp a celui d'une date formalise
            const milliseconds = this.state.donnee[0].record_timestamp * 1000
            const dateObject = new Date(milliseconds)
            const humanDateFormat = dateObject.toLocaleString()
            this.setState({ time: humanDateFormat })

        }) 
    }

    //Amene à la page map avec les bonne coordonees
    _goToYosemite(lag, long) {
        openMap({ latitude: lag, longitude: long });
        console.log(this.state.time)
      }

    render() {
        
        return (
            
            <View style={styles.content}>

                <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 10}}><Text style={{ fontSize: 18 }}>Your Vehicles</Text></View>
                
                <View style={{ flex: 2 }}>
                    
                    <View style={{ flexDirection: 'row'}}>

                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center"}}><Image style={styles.imgVoiture} source={{uri: this.props.navigation.state.params.voiture[1]}}></Image></View>
                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center"}}>
                            <Text style= {{fontSize: 20, marginBottom: 10}}>Immatriculation</Text>
                            <Text style= {{fontSize: 20}}>{this.props.navigation.state.params.voiture[2]}</Text>
                        </View>

                    </View>
                    
                </View>
                
                <View style={{ flex: 5}}>
                    
                    <View style={{ flex: 1, alignItems: "center"}}>
                        <Text style= {{fontSize: 15}}>Date of last location </Text>
                        <Text style= {{fontSize: 15}}>{this.state.time}</Text>
                    </View>
                    
                    <View style={{ flex: 2, alignItems: "center"}}>
                        <Image style={styles.imgMap} source={{uri: "https://previews.123rf.com/images/bobnevv/bobnevv1602/bobnevv160200120/52213255-ic%C3%B4ne-d-emplacement-de-la-carte-carte-de-localisation-logo-carte-de-localisation-symbole-ic%C3%B4ne-emplacem.jpg"}}></Image>
                    </View>
                    
                    <View style={{ flex: 2}}>
                        <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingTop: 70}}><Button width = "15" onPress={() => this._goToYosemite(this.state.lag, this.state.long)} title="Open the map" /></View>
                            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingTop: 70}}><Button onPress={() => this.componentDidMount()} title="Update"/></View>
                        </View>
                    </View>

                </View>
               

            </View>
        )
    }
}

const styles = StyleSheet.create({

    content: {
        flex: 1,
        //alignItems: 'center',
    },
    imgVoiture: {
        height: 150,
        width: 150
    },
    imgMap: {
        height: 200,
        width: 200
    }

})
export default Vehicle
